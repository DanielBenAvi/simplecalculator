import PySimpleGUI as gui
program_name = 'Claculator'
theme_engine = ['menu',['Black', 'BlueMono', 'BluePurple', 'BrightColors', 'DarkBlue','random']]

def create_window(theme):
    gui.theme(theme)
    gui.set_options(font='Ariel 24')

    layout = [[gui.Text("",font='Ariel 24',justification='center',expand_x=True,pad=(5,10),right_click_menu = theme_engine,key='-TEXT-')],
            [gui.Button("Clear",expand_x=True),gui.Button("Enter",expand_x=True)],
            [gui.Button("7",size=(3,1)),gui.Button("8",size=(3,1)),gui.Button("9",size=(3,1)),gui.Button("*",size=(3,1))],
            [gui.Button("4",size=(3,1)),gui.Button("5",size=(3,1)),gui.Button("6",size=(3,1)),gui.Button("/",size=(3,1))],
            [gui.Button("1",size=(3,1)),gui.Button("1",size=(3,1)),gui.Button("3",size=(3,1)),gui.Button("-",size=(3,1))],
            [gui.Button("0",expand_x=True),gui.Button(".",size=(3,1)),gui.Button("+",size=(3,1))]]
    return gui.Window(program_name,layout)

window = create_window('Black')

currnt_num = []
full_operetion = []


while True:
    event, values = window.read()

    if event == gui.WIN_CLOSED:
        break
    
    if event in theme_engine[1]:
        window.close()
        window = create_window(event)
        
    if event in ['0','1','2','3','4','5','6','7','8','9','.']:
        currnt_num.append(event)
        num_string = ''.join(currnt_num)
        window['-TEXT-'].update(num_string)
        
        
    if event in ['*','/','+','-']:
        full_operetion.append(''.join(currnt_num))
        currnt_num = []
        full_operetion.append(event)
        window['-TEXT-'].update("")
        
        
    if event == 'Enter':
        full_operetion.append(''.join(currnt_num))
        res = eval(''.join(full_operetion))
        window['-TEXT-'].update(res)
        full_operetion = []
    
    if event == 'Clear':
       window['-TEXT-'].update("") 
       full_operetion = []
       currnt_num = []
    
      
window.close()
